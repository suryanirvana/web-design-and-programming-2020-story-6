from django.shortcuts import render, redirect
from .forms import *
from .models import Mood

def index(request):
    if request.method == "POST":
        form = Mood_Input(request.POST)
        if (form.is_valid()):
            new_mood = Mood()
            new_mood.mood = form.cleaned_data['mood']
            new_mood.save()
        return redirect('/')
    else:
        form = Mood()
    mood = Mood.objects.all()
    response = {'mood': mood}
    return render(request, 'index.html', response)
