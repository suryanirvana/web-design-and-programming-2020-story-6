from django.db import models

# Create your models here.
class Mood(models.Model):
    mood = models.CharField(max_length=40, primary_key=True)
    timestamp = models.DateTimeField(auto_now_add = True, null = True)

    def __str__(self):
        return self.mood
