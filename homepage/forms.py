from django import forms
from .models import Mood

class Mood_Input(forms.Form):
    # class Meta:
    #     model = Mood
    #     fields = ['mood']
    #     widgets = {
    #         'mood': forms.TextInput(attrs = {'class': 'form-control'}),
    #     }

    mood = forms.CharField(widget=forms.TextInput(attrs={
        'class' : 'form-control',
        'placeholder' : 'Enter your current mood here',
        'type' : 'text',
        'maxlength' : '40',
        'required' : True,
        'label' : '',
    }))
