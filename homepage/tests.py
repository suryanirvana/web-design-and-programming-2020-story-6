from django.test import TestCase, Client
from django.urls import resolve
from django.apps import apps
from .apps import *
from .views import index
from .models import Mood
from .forms import Mood_Input
# Create your tests here.
class StoryUnitTest (TestCase):
    def test_story_url_is_exist(self):
        response = Client().get('')
        self.assertEqual(response.status_code, 200)

    def test_story_using_template(self):
        response = Client().get('')
        self.assertTemplateUsed(response, 'index.html')

    def test_story_using_index_func(self):
        found = resolve('/')
        self.assertEqual(found.func, index)

    def test_contains_question_in_english(self):
        response = Client().get('')
        response_content = response.content.decode('utf-8')
        self.assertIn("Hello, how are you?", response_content)
    
    def test_contains_question_in_spanish(self):
        response = Client().get('')
        response_content = response.content.decode('utf-8')
        self.assertIn("Hola, como estas?", response_content)

    def test_contains_question_in_french(self):
        response = Client().get('')
        response_content = response.content.decode('utf-8')
        self.assertIn("Salut, comment allez-vous?", response_content)

    def test_apps(self):
        self.assertEqual(HomepageConfig.name, 'homepage')
        self.assertEqual(apps.get_app_config('homepage').name, 'homepage')
    
    def test_model_can_create_new_mood(self):
        new_mood = Mood.objects.create(mood='mood')
        count_all_new_mood = Mood.objects.all().count()
        self.assertEqual(count_all_new_mood,1)
    
    def test_model_mood_return_mood_attribute(self):
        new_mood = Mood.objects.create(mood='mood')
        self.assertEqual(str(new_mood), new_mood.mood)

    def test_mood_form_validation_for_blank(self):
        new_mood = Mood_Input(data={'mood': ''})
        self.assertFalse(new_mood.is_valid())
        self.assertEqual(new_mood.errors['mood'], ["This field is required."])
    
    def test_form_validation_for_filled_items(self) :
        response = self.client.post('', data={'mood' : 'Mood'})
        response_content = response.content.decode()
        self.assertIn(response_content, 'Mood')

    # def test_form_name(self) :
    #     response = self.client.post('', data={'mood' : 'Mood'})
    #     new_mood = Mood.objects.get(pk=1)
    #     self.assertEqual(str(new_mood), new_mood.mood)

