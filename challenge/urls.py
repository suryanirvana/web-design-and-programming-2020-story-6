from django.urls import path
from .views import npm

app_name = 'challenge'

urlpatterns = [
    path('', npm, name='npm'),
]