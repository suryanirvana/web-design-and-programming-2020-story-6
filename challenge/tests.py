from django.test import TestCase
from django.test.client import Client

# Create your tests here.
class TDDChallenge(TestCase):
    def test_if_challenge_url_exist(self):
        response = Client().get('/challenge/')
        self.assertEqual(response.status_code, 200)

    def test_if_template_is_used(self):
        response = Client().get('/challenge/')
        self.assertTemplateUsed(response, 'npm.html')
        
    def test_if_npm_exist(self):
        response = Client().get('/challenge/')
        response_content = response.content.decode('utf-8')
        self.assertIn("1806173550", response_content)
